/*
Author: de Freitas, P.C.P
Description - Coefficient Performance
*/
#include<iostream>
using namespace std;
double mean(double data[],int length)
{
	double sum = 0.0;
	for (int i = 0; i < length; i++)
	{
		sum += data[i];
	}
	return sum / length;
}
void coefficientPerformance(double data[], int length)
{
	for (int i = 0; i < length; i++)
	{
		cout << data[i] / mean(data, length) << endl;
	}
}
int main()
{
	double NEAM20181[24] = { 10,7.5,7.5,0,8,0,10,6,8.5,0,8.25,6.5,5,8.5,0,5.5,0,9,8.5,9,0,0,5.5,0 };
	coefficientPerformance(NEAM20181, 24);
	cout << endl;

	double NEAM20182[8] = { 7,8.25,8,8.75,6.5,2.25,7,4.5 };
	coefficientPerformance(NEAM20182, 8);
	cout << endl;

	double NEES20181[11] = { 0,0,7.5,0,8,0,4.5,7.5,3.5,6.5,7 };
	coefficientPerformance(NEES20181, 11);
	cout << endl;

	double NEAAM20181[11] = { 9.0,10,10,8.0,8.5,8.25,8.5,8.5,7.5,7.5,9.0 };
	coefficientPerformance(NEAAM20181, 11);
	cout << endl;

	double NEAAM20182[5] = { 8.75,7.0,7.0,8.0,8.25 };
	coefficientPerformance(NEAAM20182,5);
	cout << endl;

	double NEAES20181[4] = { 8.0,7.5,7.5,7.0 };
	coefficientPerformance(NEAES20181, 4);
	cout << endl;

	return 0;
}